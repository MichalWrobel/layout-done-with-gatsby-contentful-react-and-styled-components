import styled from 'styled-components';

const ServiceDetail = styled.div`
	display: flex;
	justify-content: space-between;
	flex-wrap: wrap;
	span {
		display: inline-block;
		text-align: center;
		width: 164px;
		margin-top: 56px;
	}
	img {
		margin: 0;
		user-select:none;
	}
	span+div {
		float: right;
		padding: 30px;
		padding-left: 0;
		display: inline-block;
		width: 391px;
	}
	div {
		width:555px;
		height: 182px;
		background: #fcfcfc;
		margin-bottom: 30px;
	}
	h4 {
		margin-bottom: 20px;
		color: #333333;
		font-family: 'Raleway';
		font-size: 18px;
		font-weight: 700;
	}
	p {
		margin: 0;
		color: #777777;
		font-family: 'Open Sans';
		font-size: 13px;
		font-weight: 400;
		line-height: 20px;
	}
`;

export default ServiceDetail;
