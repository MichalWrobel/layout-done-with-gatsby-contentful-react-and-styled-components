import styled from 'styled-components';

const Slogan = styled.div`
	padding-top: 20px;
	border-top: 4px solid #fdc300;
    border-image: linear-gradient(to right,rgba(0,0,0,0) 5px,#fdc300 5px,#fdc300 66px,rgba(0,0,0,0) 0px);
    border-image-slice: 1;
	text-shadow: 1px 1px 3px #aaaaaa;
	color: #ffffff;
	font-family: 'Raleway';
	font-size: 72px;
	font-weight: 800;
	line-height: 60px;
	text-transform: uppercase;
    width: 504px;
    word-wrap: normal;
    margin-top: calc(50% - 355px);
    margin-bottom: 154px;
    user-select: none;
`;
export default Slogan;