import styled, { css } from 'styled-components';

const MenuItem = styled.li`
	
	padding: 14px;
	line-height: 48px;
	list-style: none;
	display:inline-block;
	&:hover {
		border-bottom: 5px solid;
		border-color: #006db7;
	}
	${(props) => props.active && css`
		border-bottom: 5px solid;
		border-color: #006db7;
	`}
	a, a:link, a:visited, a:hover, a:active {
		color: #333333;
		text-decoration: none;
		font-family: Raleway;
		font-size: 13px;
		font-weight: 700;
		line-height: 22px;
		text-transform: uppercase;

	}
`;
export default MenuItem;