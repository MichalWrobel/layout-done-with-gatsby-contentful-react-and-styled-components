import styled from 'styled-components';

const SearchButton = styled.div`
	cursor: pointer;
	text-align: center;
    display: inline-block;
	width: 35px;
	height: 35px;
	border-radius: 50%;
	background: #006db7;
	transition: background .2s ease;
	&:hover {
		background: #0083db;
	}
	img {
		display: block;
		position: relative;
		top: 10.5px;
	    margin: 0 auto;
	}
`;
export default SearchButton;