import styled from 'styled-components';

const ArticleBox = styled.span`
	display:inline-block;
	width: 360px;
	box-shadow: 0 1px 3px #dddddd;
	button {
		float:right;
	    margin-top: 182px;;
	}
	div {
		background: #dededd;
		height: 216px;
	}
	div~div {
		padding: 31px 29px 31px 28px;
		height: 184px;
		background: #ffffff;
	}
	h3 {
		margin-bottom: 23px;
		padding-top: 9px;
		border-top: 4px solid #fdc300;
		border-image: linear-gradient(to right,#fdc300 30px,rgba(0,0,0,0) 0px);
		border-image-slice: 1;

		color: #333333;
		font-family: 'Raleway';
		font-size: 18px;
		font-weight: 800;
		text-transform: uppercase;
	}
	p {
		color: #777777;
		font-family: 'Open Sans';
		font-size: 13px;
		font-weight: 400;
		line-height: 20px;
	}
`;
export default ArticleBox;