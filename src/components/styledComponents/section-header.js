import styled from 'styled-components';

const SectionHeader = styled.div`
	margin-top: 69px;
	margin-bottom: 40px;
	text-align: center;
	font-weight: 600;
	h2 {
		margin-bottom: 0;
		color: #333333;
		font-family: 'Raleway';
		font-size: 30px;
	}
	p {
		margin-bottom: 0px;
		color: #888888;
		font-family: 'Raleway';
		font-size: 13px;
		text-transform: uppercase;
	}
	div {
		margin-bottom: 40px;
		margin: 0 auto;
		width: 30px;
		height: 3px;
		background: #fdc300;
	}
`;

export default SectionHeader;