import styled, { css } from 'styled-components';
const Button = styled.button`
    padding-left: 19px;
    background: linear-gradient(to right, #1b2936 135px,#2f3840 30px);
    cursor: pointer;
    border: none;
    color: white;
    width: 165px;
    height: 34px;
    font-family: Raleway;
    font-size: 13px;
    font-weight: 700;
    line-height: 20px;
    text-transform: uppercase;
    user-select:none;
    &:hover {
    	background: linear-gradient(to right, #006db7 135px,#037ed1 30px);
    }

	${(props) => props.cta && css`
		padding-left: 0;
		width: 216px;
		height: 49px;
		border-radius: 25px;
		background: #fdc300;
		color: #ffffff;
		font-family: Raleway;
		font-size: 13px;
		font-weight: 700;
		line-height: 20px;
		text-transform: uppercase;
		margin-top: 50px;
		margin-left: calc(50% - 108px);
		transition: background .2s ease;
		&:hover {
		    border: 2px solid #fdc300;
		    background: none;
		    color: #fdc300;
		}
	`}
`;
export default Button;