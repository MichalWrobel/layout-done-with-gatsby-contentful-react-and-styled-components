import styled, { css } from 'styled-components';

const Section = styled.section`
	@import url('https://fonts.googleapis.com/css?family=Open+Sans|Raleway:400,600,700,800');
	font-family: Raleway;
	background: white;
	height: 636px;
	width: 100%;
	${(props) => props.primary && css`
		background: #122636;
		background: linear-gradient(to bottom, #122636 854px,#1b2936 355px);
		height: 1189px;
	`}
`;
export default Section;