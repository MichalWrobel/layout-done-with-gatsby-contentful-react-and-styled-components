import styled from 'styled-components';

const Nav = styled.nav`

	position: relavite;
	right: 0;
	left: 0;
	margin: auto;
	width: 100%;
	transform: translateY(43px);
	height: 81px;
	box-shadow: 0 -8px 0 rgba(255, 255, 255, 0.2), inset 1px 1px 0 rgba(201, 202, 202, 0.75);
	border-radius: 3px;
	background-image: linear-gradient(to top, #fcfcfc 0%, #ffffff 100%);
	user-select:none;
`;
export default Nav;