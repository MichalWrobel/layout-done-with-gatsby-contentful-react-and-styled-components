import styled from 'styled-components';

const Arrow = styled.i`
	border: solid white;
	border-width: 0 3px 3px 0;
	display: inline-block;
	padding: 2px;
	margin-left: 25px;
	transform: rotate(-45deg);
	float: right;
    margin-top: 6.275px;
    margin-right: 5.325px;
`;

export default Arrow;