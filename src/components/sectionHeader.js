import React from 'react';
import PropTypes from 'prop-types';
import SectionHeader from './styledComponents/section-header';

class Header extends React.Component {
	render() {
		return (
			<SectionHeader>
				<h2>{this.props.title}</h2>
				<p>{this.props.subtitle}</p>
				<div/>
			</SectionHeader>
		);
	}
}

Header.propTypes = {
	title: PropTypes.string.isRequired,
	subtitle: PropTypes.string.isRequired
};

export default Header;