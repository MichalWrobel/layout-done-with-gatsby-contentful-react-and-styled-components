import React from 'react';
import PropTypes from 'prop-types';
import ServiceDetail from './styledComponents/service-detail';

class Services extends React.Component {
	render() {
		const serviceDetails = this.props.data.map((item, index) => {

			return (
				<div key = {index}>
					<span>
						<img src = {item.image} alt = {item.alt}/>
					</span>
					<div>
						<h4>{item.title}</h4>
						<p>{item.text}</p>
					</div>
				</div>
			);
		});

		return (
			<ServiceDetail>{serviceDetails}</ServiceDetail>
		);
	}
}

Services.propTypes = {
	data: PropTypes.array.isRequired
};

export default Services;