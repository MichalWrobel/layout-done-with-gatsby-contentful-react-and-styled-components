import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import ArticleBox from './styledComponents/article-box';
import Button from './styledComponents/button';
import Arrow from './styledComponents/arrow';

class ContentBox extends React.Component {
	render() {
		const Wrapper = styled.div`
			display: flex;
			justify-content: space-between;
		`;
		const boxes = this.props.services.map((item, index) => {

			return(
				<ArticleBox key = {index}>
					<div><Button>{item.butttonLabel}<Arrow/></Button></div>
					<div>
						<h3>{item.title}</h3>
						<p>{item.text}</p>
					</div>
				</ArticleBox>
			);
		});

		return (
			<Wrapper>{boxes}</Wrapper>
		);
	}
}

ContentBox.propTypes = {
	services: PropTypes.array.isRequired,
};
export default ContentBox;