import React from 'react';
import Link from 'gatsby-link';
import Nav from './styledComponents/nav.js';
import MenuItem from './styledComponents/menu-item.js';
import Logo from './logo.js';
import SearchButton from './styledComponents/search-button';
import styled from 'styled-components';

class Navigation extends React.Component {
	render() {
		const menuItems = this.props.menuItems.map((item, index) => {
			if (index === 0) {
				return <MenuItem active key = {index}><Link to = '/'>{item}</Link></MenuItem>;
			}

			return <MenuItem key = {index}><Link to = '/'>{item}</Link></MenuItem>;
		});
		const Wrapper = styled.ul`
			display: inline-block;
			height: 100%;
			padding: 0;
			margin: 0;
			float: right;
		`;
		const SearchWrapper = styled.div`
			padding: 24px 30px 22px 18px;
			float: right;
		`;

		return (
			<header>
				<Nav>
					<Logo logo = {this.props.logo} alt = {this.props.altLogo}/>
					<SearchWrapper><SearchButton><img src='/img/shape-6.svg' alt='Search'/></SearchButton></SearchWrapper>
					<Wrapper>{menuItems}</Wrapper>
				</Nav>
			</header>
		);
	}
}
export default Navigation;