import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

class Logo extends React.Component {
	render() {

		const Wrapper = styled.div`
			display: inline-block;
			float: left;
			height: 42px;
			padding-top: 18px;
			padding-left: 30px;
		`;

		const Link = styled.a`

			&:link, &:visited, &:hover, &:active {
				text-decoration: none;
			}
		`;

		return (
			<Wrapper>
				<Link href = '/'>
					<img src = {this.props.logo} alt = {this.props.altLogo}/>
				</Link>
			</Wrapper>
		);
	}
}

Logo.propTypes = {
	logo: PropTypes.string.isRequired,
	alt: PropTypes.string.isRequired
};

export default Logo;