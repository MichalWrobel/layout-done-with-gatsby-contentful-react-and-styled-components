import React from 'react';
import * as contentful from 'contentful';
import Section from '../components/styledComponents/section.js';
import Container from '../components/styledComponents/container.js';
import Button from '../components/styledComponents/button.js';
import Slogan from '../components/styledComponents/slogan.js';
import Navigation from '../components/navigation';
import ContentBox from '../components/contentBox';
import Header from '../components/sectionHeader';
import Services from '../components/services';

class Home extends React.Component {
	constructor() {
		super();
		this.state = {
			logo: {
				alt: '',
				url: ''
			},
			menuItems: [],
			sectionHeader: {
				title: '',
				text: ''
			},
			slogan: '',
			ctaButtonLabel: '',
			services: [],
			servicesDetails: []
		};
	}
	getSpecificEntries(collection, entry, special) {
		const wantedItems = [];
		Object.keys(collection).forEach((field) => {
			if (field.startsWith(entry)) {
				if (special) {
					wantedItems.push(collection[field].fields.file.url);
				} else {
					wantedItems.push(collection[field]);
				}
			}
		});

		return wantedItems;
	}
	combineArray(arr1, arr2, arr3, label1, label2, label3) {
		const wantedItems = [];
		if (arr1.length !== arr2.length) {
			console.error('Arrays must have same size');

			return;
		}
		for (let i = 0; i < arr1.length; i++) {
			wantedItems.push({ [label1]: arr1[i], [label2]: arr2[i], [label3]: arr3[i] });
		}

		return wantedItems;
	}
	componentDidMount() {
		const client = contentful.createClient({
			space: '8laluq8hzwyt',
			accessToken: 'df3dd13d644a67c57133773ac662fb7ec71d6f4c0f5c0fb2bd5a8c641ce78302'
		});
		client.getEntries({ content_type: 'pageSection' }).then((entries) => {

			const fields = entries.items[0].fields;
			const homeServiceTitles = this.getSpecificEntries(fields, 'serviceTitle');
			const homeServiceDescriptions = this.getSpecificEntries(fields, 'serviceDescription');
			const homeSerbiceButtonLabels = this.getSpecificEntries(fields, 'serviceButtonLabel');

			this.setState({
				logo: {
					alt: entries.includes.Asset[0].fields.description,
					url: entries.includes.Asset[0].fields.file.url
				},
				menuItems: this.getSpecificEntries(fields, 'menu'),
				slogan: fields.slogan,
				services: this.combineArray(homeServiceTitles, homeServiceDescriptions, homeSerbiceButtonLabels, 'title', 'text', 'butttonLabel'),
				ctaButtonLabel: fields.callToActionButtonLabel

			});
		});
		client.getEntries({ content_type: 'pageSection2' }).then((entries) => {
			const fields = entries.items[0].fields;
			const serviceDescs = this.getSpecificEntries(fields, 'serviceDesc');
			const serviceTitles = this.getSpecificEntries(fields, 'serviceTitle');
			const serviceImages = this.getSpecificEntries(fields, 'serviceIcon', true);

			this.setState({
				sectionHeader: {
					title: fields.title,
					text: fields.subtitle
				},
				servicesDetails: this.combineArray(serviceImages, serviceTitles, serviceDescs, 'image', 'title', 'text')
			});
		});
	}

	render() {

		return(
			<div>
				<Section primary>
					<Container>
						<Navigation
							logo = {this.state.logo.url}
							altLogo = {this.state.logo.alt}
							menuItems = {this.state.menuItems}
						/>
						<Slogan>{this.state.slogan}</Slogan>
						<ContentBox services = {this.state.services}/>
						<Button cta>{this.state.ctaButtonLabel}</Button>
					</Container>
				</Section>
				<Section>
					<Container>
						<Header
							title = {this.state.sectionHeader.title}
							subtitle = {this.state.sectionHeader.text}
						/>
						<Services data = {this.state.servicesDetails}/>
					</Container>
				</Section>

			</div>
		);
	}
}

export default Home;