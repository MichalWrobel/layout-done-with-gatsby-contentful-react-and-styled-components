module.exports = {
	siteMetadata: {
		title: 'Gatsby Default Starter',
	},
	plugins: [
		{
			resolve: 'gatsby-source-contentful',
			options: {
				spaceId: '8laluq8hzwyt',
				accessToken: 'df3dd13d644a67c57133773ac662fb7ec71d6f4c0f5c0fb2bd5a8c641ce78302',
			},
		},
		'gatsby-plugin-react-helmet'
	],
}
